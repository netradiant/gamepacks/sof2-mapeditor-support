# Soldier of Fortune Ⅱ: Double Helix map editor support

This is a gamepack for [NetRadiant](https://netradiant.gitlab.io) providing support for Soldier of Fortune Ⅱ: Double Helix.

This gamepack is based on the game pack provided by NetRadiant-custom.

More stuff may be imported from http://svn.icculus.org/gtkradiant-gamepacks/Sof2Pack/ in the future.
